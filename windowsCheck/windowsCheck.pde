
/**
 * Bachelor Thesis
 * Matija Pevec 0036479246
 * Software development for machine-to-machine communication battery powered devices
 *
 * Scenario: Windows Check
 *
 * Required equipment:
 *   Libelium Waspmote
 *   Waspmote Events Sensor Board v2.0
 *   Sensors:
 *     1. Hall Effect Sensor              - Socket 1 (polarity doesn't matter)
 *     2. Hall Effect Sensor              - Socket 2 (polarity doesn't matter)
 *     3. Hall Effect Sensor              - Socket 3 (polarity doesn't matter)
 */

#include <stdio.h>
#include <WaspSensorEvent_v20.h>
#include <WaspXBee802.h>
#include <WaspFrame.h>

#define HALL_EFFECT 10

char RX_ADDRESS[] = "000000000000FFFF";
char WASPMOTE_ID[] = "WindowsCheck";

float hall_effect_value1;
float hall_effect_value2;
float hall_effect_value3;

char* timeRTCformat;


void setup() {
  
  PWR.ifHibernate();
  
  USB.ON();
  
  // Turn on the sensor board
  SensorEventv20.ON();
  
  // Turn on the RTC
  RTC.ON();
  
  frame.setID( WASPMOTE_ID );
  
  // init XBee
  //xbee802.ON();
    
  // set sleep mode
  //xbee802.setSleepMode(1);

  timeRTCformat = reformatTime((unsigned long) HALL_EFFECT * 1000);

  USB.println(F("Starting the measurements..."));
  
  
  
}


void loop() {
  
  if( intFlag & HIB_INT )
  {
    
    hibInterrupt();
  }
  
  
  
  
  USB.printf("Entering into \"Hibernate\" mode for %s\n\n", timeRTCformat);
  PWR.hibernate(timeRTCformat, RTC_OFFSET, RTC_ALM1_MODE1);
  
}

void hibInterrupt() {
  
  USB.println(F("---------------------"));
  USB.println(F("Hibernate Interruption captured"));
  USB.println(F("---------------------"));

  frame.createFrame(ASCII);
  frame.addSensor(SENSOR_BAT, PWR.getBatteryLevel());
  
  USB.printf("-----------------------------\n");
  USB.printf("Reading sensor values:\n");
  
  delay(1000);
  
  hall_effect_value1 = SensorEventv20.readValue(SENS_SOCKET1) > 0 ? 1 : 0;
  hall_effect_value2 = SensorEventv20.readValue(SENS_SOCKET2) > 0 ? 1 : 0;
  hall_effect_value3 = SensorEventv20.readValue(SENS_SOCKET3) > 0 ? 1 : 0;
  
  frame.addSensor(SENSOR_HALL, hall_effect_value1);
  frame.addSensor(SENSOR_HALL, hall_effect_value2);
  frame.addSensor(SENSOR_HALL, hall_effect_value3);
  
  if (hall_effect_value1) {
    USB.println("  Hall Effect 1: CLOSED ");
  } else {
    USB.println("  Hall Effect 1: OPENED ");
  }
  
  if (hall_effect_value2) {
    USB.println("  Hall Effect 2: CLOSED ");
  } else {
    USB.println("  Hall Effect 2: OPENED ");
  }
  
  if (hall_effect_value3) {
    USB.println("  Hall Effect 3: CLOSED ");
  } else {
    USB.println("  Hall Effect 3: OPENED ");
  }
  
  frame.showFrame();
  //xbee802.send( RX_ADDRESS, frame.buffer, frame.length );
  
  intFlag &= ~(HIB_INT);
  delay(1000); 
}


// Reformats time in the RTC format. Supports only
// time that is no longer than 1 day ( only hours, 
// minutes and seconds). The time format that function
// returns will look like this: "00:01:50:10\0"
char* reformatTime(unsigned long timeInMilis) {
  unsigned long cutTime;
  int hours;
  int minutes;
  int seconds;

  char* text = (char *) malloc(13);

  cutTime = timeInMilis;

  // cutting miliseconds
  cutTime /= 1000;

  // getting seconds
  seconds = cutTime % 60;
  cutTime /= 60;

  // getting minutes
  minutes = cutTime % 60;
  cutTime /= 60;

  // getting hours
  hours = cutTime % 60;

  sprintf(text, "00:%.2d:%.2d:%.2d\0", hours, minutes, seconds);
  return text;
}


