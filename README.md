#Bachelor Thesis#
Name: ***Matija Pevec*** 

JMBAG: **0036479246**

##***Software development for machine-to-machine communication battery powered devices***##

###Description:###
*There is a growing number of simple devices in modern Internet usage. A typical example of such devices are sensor nodes, which are commonly battery powered and used to transmit various data from sensors. Along with development of energy efficient hardware and communication protocols, there is also an increasing need to develop energy efficient software in order to extend the duration of batteries. Your task is to explore capability of the Libelium Waspmote platform and develop energy efficient software for gathering data from a sensor node operating in the smart-home environment. The sensor node collects data from a temperature sensor, humidity sensor, light sensor, presence detector and door open sensor. The presence detector and the door open sensor are activated when a certain event occurs, while the temperature sensor, the humidity sensor and the light sensor should be activated periodically. The node on standby should operate in the low-energy consumption mode so as to extend the duration of batteries as much as possible. The Department of Telecommunications will provide required literature, as well as all other equipment necessary for conducting this task. 
*



###Required equipment:###

* Libelium Waspmote
* Waspmote Events Sensor Board v2.0

###Scenario : Smart Room###
* Sensors:
    * Luminosity Sensor              - **Socket 1** (polarity doesn't matter)
    * Hall Effect Sensor               - **Socket 2 & 3**  (single wire -> +3.3V (PIN 3 or 6); splitted wire -> Output (PIN 4 and 5))
    * Temperature Sensor (MCP9700A)  - **Socket 5** (flat side is faced towards the board)
    * Humidity Sensor 3.3V (808H5V5) - **Socket 6** (perforated side is faced towards the board)
    * Presence Sensor (PIR)          - **Socket 7** (side with capsule is faced on the opposite side of the board)

###Scenario : Windows Check###
* Sensors:
    * Hall Effect Sensor 1             - **Socket 1** (polarity doesn't matter)
    * Hall Effect Sensor 2             - **Socket 2** (polarity doesn't matter)
    * Hall Effect Sensor 3             - **Socket 3** (polarity doesn't matter)